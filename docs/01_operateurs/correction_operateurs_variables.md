---
author: Dupuits
title: correction opérateurs et variables
tags:
  - opérateurs/variables
---


# Opérateurs et variables

## Exercice 1

- opérateur de division entière  :

```python
20 // 3 = 6
```

- opérateur de modulo (reste d’une division euclidienne)

```python
20 % 3 = 2
```

- opérateur exposant est le plus prioritaire

```python
5 **2 % 3 = 25 % 3 = 1
5 % 2** 3 = 5 % 8 = 5
```

## Exercice 2

- multiplication entre deux str impossible

```python
'ab' * 'cd' = erreur
'ab' * '3' = erreur
```

- multiplication entre un str et un entier

```python
'ab' * 3 = 'ababab'
```

- addition entre deux str:concaténation de chaînes de caractères

```python
    'ab'+'cd' = 'abcd'
    'ab'+'3' = 'ab3'
```

- addition entre un str et un entier : impossible

```python
    'ab'+3 = erreur
```

## Exercice 3

> La fonction type() permet d’afficher le type d’une variable. Les fonctions  int(), str(), bool() et float() permettent de changer le type d’une variable. Le type d’une variable peut également changer implicitement après certaines opérations sur cette variable.

## Exercice 4

- Transformation de 3.4 en entier

```python
    int(3.4) = 3
```

- Addition entre deux entiers

```python
    int('1') + 1 = 2
```

- Transformation d’une chaîne de caractères en float

```python
    float('3.5') = 3.5
```

- Construction d’une chaîne de caractères et transformation en float

```python
    float('.' + '1' * 2) = 0.11
```

- Concaténation de deux chaines de caractères

```python
    str(2) + str(1) = '21'
```

- Transformation de 2 en booléen qui donne True puis transformation de True en entier qui donne 1

```python
    int(bool(2)) = 1
```

- 'False' est interprété comme une chaîne non vide donc la fonction bool renvoie True

```python
    bool('False') = True
```

- Transformation de l’entier 0 en booléen qui donne False mais la transformation de la str '0' donne True

```python
    bool(0), bool('0') = False, True
```
