---
author: Dupuits
title: correction interaction avec l'utilisateur
tags:
  - print/input
---

# Interaction avec l'utilisateur

## Exercice 1 : affichage d'une division

```python
# pas de correction
```

## Exercice 2 : Somme des chiffres d’un nombre

```python
n = int(input("entrez un nombre entre 0 et 999 : "))

# calcul des différents chiffres
centaine  =n // 100
dizaine = (n % 100)  // 10
unite = (n % 10)

#calcul de la somme
somme = centaine + dizaine + unite

print("la somme des chiffres de n est : ", somme)
```

## Exercice 3 : mesure du temps

```python
h = float(input("entrer une durée en décimal : "))

#calcul et affichage des heures
heure = int(h)
print("heure : " , heure)

#calcul et affichage des minutes
minutes = int((h - heure) * 60)
print("minutes : ", minutes)

#calcul et affichage des secondes
secondes = int(((h -  heure) - minutes / 60) * 3600)
print("secondes : ", secondes)
```

## Exercice 4 : nombre magique

```python
n = int(input("entrer un nombre n : "))
resultat = (2 * n + 84) / 2 - n
print("resultat : ", resultat)
```

## Exerice 5 : les caractères d'échappement

```python
# pas de correction
```
