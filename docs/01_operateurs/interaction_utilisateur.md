---
author: Dupuits
title: Interaction avec l'utilisateur
tags:
  - print/input
---
# Interaction avec l'utilisateur

## Exercice 1 : affichage d'une division

Ecrire un programme qui demande un nombre x et qui écrit sous avec un format spécifique (de type str) le résultat de la division entière de x par 11 (voir exemple ci-dessous).

!!! example "Exemple"

    entrer un nombre x : 149
    149 = 13 x 11 + 6


> il faut utiliser la fonction input pour faire l'acquisition d'un nombre, les opérateurs % et // pour obtenir le reste et le quotient et l'opérateur de concaténation pour construire une chaine de caractères.

## Exercice 2 : Somme des chiffres d’un nombre

On considère un programme qui demande un nombre entier n tel que n est compris entre 0 et 999. L’objectif est de réaliser un programme qui détermine la somme des chiffres du nombre.

**Comportement attendu du programme**

```python
entrez un nombre compris entre 0 et 99 : 247
la somme des chiffres de n est : 13

entrez un nombre compris entre 0 et 99 : 109
la somme des chiffres de n est : 10

entrez un nombre compris entre 0 et 99 : 25
la somme des chiffres de n est : 7
```

1. Déterminer et lister le nombre de variables nécessaires pour réaliser ce programme.
2. Pour n=456, déterminer le résultat des calculs suivants:

!!! example 

    ```python
    c1 = n // 100
    c2 = n % 100
    ```

3. Écrire et tester le programme demandé.

## Exercice 3 : mesure du temps

Il existe deux manières d’écrire une durée, soit en décimal soit avec le système heure minutes seconde. Par exemple 1,5h correspond à 1h et 30mn. Écrire un programme qui attend une valeur de durée exprimée en décimal et qui donne cette mème durée mais exprimée en heure ; minutes ; secondes.

**Comportement attendu du programme**

```python
entrez une durée en décimal : 0.65
heure : 0
minutes : 39
secondes : 0

entrez une durée en décimal : 1.25
heure : 1
minutes : 15
secondes : 0

entrez une durée en décimal : 2.33
heure : 2
minutes : 19
secondes : 48

```

## Exercice 4 : nombre magique

un mentaliste vous propose l’expérience suivante :

- penser à un nombre entier n
- doubler ce nombre n
- ajouter 84 au résultat
- diviser le tout par deux
- soustraire n au résultat

Le mentaliste assure connaître le résultat sans pour autant connaître le nombre n de départ. Ecrire un programme qui affiche le résultat et tester ce programme avec différentes valeurs de n.

## Exerice 5 : les caractères d'échappement

1. Réaliser la séquence de commandes Python suivante :

    ```python
    print(‘test’)
    print("test")
    print(‘test1’ test2) 
    print("aujourd’hui")
    print("il répond : "oui"")
    print(‘il répond : "oui"’)
    print("essai:/")
    print("essai:\")
    ```

2. Les affichages obtenus correspondent-ils aux résultats attendus ?
3. Quels est le rôle des guillemets et apostrophes ?
4. Quels sont les caractères qui ont un comportement inattendu

    Pour coder certains aspects d’un affichage, Python utilise un caractère spécifique nommé caractère d’échappement. Ce caractère est l’antislash (contre barre), il est toujours suivi d’un autre caractère qui définit une spécificité. Le tableau ci-dessous résume quelques codes d’échappement.

    |code   |role                                              |
    |:-----:|:------------------------------------------------:|
    |\n     |insertion d’un passage à la ligne dans une chaîne |
    |\t     |insertion d’une tabulation dans une chaîne        |
    |\"     |insertion d’un guillemet dans une chaîne          |
    |\’     |insertion d’un apostrophe dans une chaîne         |
    \\      |insertion d’un antislash dans une chaîne          |

5. Ecrire en une seule instruction « print » l’affichage ci-dessous

```python
Aujourd'hui
C'est l’utilisation de l’ "anti slash"
	Il se note \
```
