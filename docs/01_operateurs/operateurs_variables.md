---
author: Dupuits
title: Opérateurs et variables
---

# Opérateurs et variables

## Exercice 1

En utilisant l’invite de commande python, exécuter les opérations suivantes et justifier les valeurs.  

```python
>>> 20 // 3 
>>> 20 % 3
>>> 5 ** 2 % 3 
>>> 5 % 2 ** 3 
```

## Exercice 2

En utilisant l’invite de commande python, exécuter les opérations suivantes et conclure sur le rôle des opérateurs + et * sur des objets de type str.  

```python
    'ab' * 'cd' 
    'ab' * '3'
    'ab' * 3
    'ab' + 'cd' 
    'ab' + '3'
    'ab' + 3
```

## Exercice 3

En utilisant la console python, exécuter les opérations suivantes et indiquer le rôle des instructions type(), int(), str(), bool() et float()

```python 
    a = 1
    type(a)
    b = 2.5
    type(b)
    a = a + b
    type(a)
    a = a - b
    type(a)
    a = int(a)
    type(a)
    a = str(a)
    type(a)
    a = bool(a)
    type(a)
```

!!! info "Mon info"

    Ma belle info qui doit être indentée




## Exercice 4

En utilisant la console python, exécuter et interpréter les opérations suivantes

```python linenums='1'
    int(3.4) 
    int('1')+1
    float('3.5')
    float('.' + '1' * 2)
    str(2) + str(1)
    int(bool(2))
    bool('False')
    bool(0), bool('0')
```

!!! warning "Remarque"

    Ma remarque qui doit être indentée

!!! example "Exemple"

    Mon exemple indenté
