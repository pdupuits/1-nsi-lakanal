---
author: Dupuits
title: 🏡 1nsi Lakanal
---

# Les activités de première NSI

😊  Bienvenue 😊

Cet espace regroupe toutes les activités effectuées dans le cadre de l'enseignement de spécialité de première NSI ainsi que des éléments de correction.

Les chapitres abordés cet année sont :
- chapitre 01 : les opérateurs
- chapitre 02 : les structures algorithmiques




